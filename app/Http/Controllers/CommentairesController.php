<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Models\Commentaire;


class CommentairesController extends Controller {
  /**
   * Affichage des commentaires
   */
  public function index(){
    return response()->json(Commentaire::all());
  }

  /**
     * Ajout d'un commentaire
     * @param  integer $commentaire [commentaire à ajouter]
     * @return [commentaire]        [ressources/Show.vue]
     */
  public function store(Request $request){
      $commentaire = Commentaire::create($request->all());
      return response()->json($commentaire);
  }
}
