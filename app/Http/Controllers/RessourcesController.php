<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use App\Http\Models\Ressource;
use Illuminate\Http\Request;

class RessourcesController extends Controller {
  /**
   * Affichage des ressources
   */
  public function index(){
    return response()->json(Ressource::with('categorie')->get());
  }
}
