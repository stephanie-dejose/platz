let actions = {
  setRessources({commit}) {
    axios.get('api/ressources')
         .then(reponsePHP => (commit('SET_RESSOURCES', reponsePHP.data))),
    axios.get('api/categories')
         .then(reponsePHP => (commit('SET_CATEGORIES', reponsePHP.data)))
  },
  setCategories({commit}) {
    axios.get('api/categories')
         .then(reponsePHP => (commit('SET_CATEGORIES', reponsePHP.data)))
  },
  setCommentaires({commit}) {
    axios.get('api/commentaires')
         .then(reponsePHP => (commit('SET_COMMENTAIRES', reponsePHP.data)))
  },
  createCommentaire({commit}, commentaire) {
    axios.post('api/commentaires', commentaire)
         .then(res => (
           commit('CREATE_COMMENTAIRE', res.data)))
         .catch(err => {
            console.log(err)
         })
  }
}

export default actions
