let mutations = {
  SET_RESSOURCES(state, data) {
    state.ressources = data;
  },
  SET_CATEGORIES(state, data) {
    state.categories = data;
  },
  SET_COMMENTAIRES(state, data) {
    state.commentaires = data;
  },
  CREATE_COMMENTAIRE(state, commentaire) {
    state.commentaires.unshift(commentaire)
  }
}

export default mutations
