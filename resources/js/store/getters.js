let getters = {
  getRessources(state) {
    return state.ressources;
  },
  getRessourceById(state) {
    return function(id) {
      return state.ressources.find(ressource => ressource.id == id);
    }
  },
  getCategories(state) {
    return state.categories;
  },
  getRessourcesByCategorieId(state) {
    return function(id) {
      return state.ressources.filter(ressources => ressources.categorie.id == id);
    }
    return this.ressources.filter((ressource) => {
      return ressource.titre.match(this.search);
    })
  },
  getCommentaires(state) {
    return state.commentaires;
  },
  getCommentairesByRessourceId(state) {
    return function(id) {
      return state.commentaires.filter(commentaires => commentaires.ressource_id == id);
    }
  }
}

export default getters
